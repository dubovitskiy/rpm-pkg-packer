#!/usr/bin/env bash
# Exit on first error
set -e

# Trace each step
set -x

yum install -y rpm-build wget rpmdevtools


# Get the current directory
current_dir="$(realpath "$(dirname "${0}")")"

# Keep real absolute path to the current directory
if [[ "${current_dir}" == "." ]]; then
    current_dir="$(pwd)"
fi

readonly version="1.2.4"
readonly specfile="${current_dir}/packer.spec"
readonly specfile_copy="${current_dir}/packer.copy.spec"
readonly url="https://releases.hashicorp.com/packer/${version}/packer_${version}_linux_amd64.zip"

cp "${specfile}" "${specfile_copy}"

chown "$(whoami)":"$(whoami)" $specfile_copy

rpmdev-setuptree

# Get a source file to the %{_sourcedir}
spectool \
    --get-files \
    --sourcedir \
    --define "_packer_name packer" \
    --define "_packer_version ${version}" \
    "${specfile_copy}"

rpmbuild -bb \
    --define "_packer_name packer" \
    --define "_packer_version ${version}" \
    "${specfile_copy}"

rm --recursive --force "${specfile_copy}"
mv /root/rpmbuild/RPMS/**/*.rpm /data
chmod 777 /data/*.rpm
