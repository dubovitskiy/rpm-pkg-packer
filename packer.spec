Name:    %{_packer_name}
Version: %{_packer_version}
Release: 1%{?dist}
Summary: Create machine and container images for multiple platforms
Group:   Development/Tools
License: MPLv2.0
URL:     https://www.packer.io/
Source0: https://releases.hashicorp.com/packer/%{version}/%{name}_%{version}_linux_amd64.zip

%description
Packer is a tool for creating machine and container images for
multiple platforms from a single source configuration.

%prep

%build

%install
install --directory --mode 0755 %{buildroot}%{_bindir}
unzip -o %{SOURCE0} -d %{buildroot}%{_bindir}

%files
%{_bindir}/packer

%changelog
